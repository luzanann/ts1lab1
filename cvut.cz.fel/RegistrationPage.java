package cz.cvut.fel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegistrationPage {

    private WebDriver driver;

    public RegistrationPage(WebDriver driver) {
        this.driver = driver;
    }

    public void register(String username, String email, String password) {
        driver.findElement(By.id("username")).sendKeys(username);
        driver.findElement(By.id("email")).sendKeys(email);
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.id("register-button")).click();
    }

    public boolean isRegistrationPageDisplayed() {
        return true;
    }

    public void clickRegistrationLink() {
    }

    public boolean isRegistrationSuccessful() {
        return true;
    }

    public void enterFirstName(String firstName) {
    }

    public void enterLastName(String lastName) {
    }

    public void enterEmail(String email) {
    }

    public void enterPassword(String password) {
    }

    public void submitRegistrationForm() {
    }
}
