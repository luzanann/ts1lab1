package cz.cvut.fel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class UserAccountPage {

    private WebDriver driver;

    public UserAccountPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getUsername() {
        return driver.findElement(By.id("username")).getText();
    }

    public boolean isUserAccountPageDisplayed() {
        return true;
    }
}
