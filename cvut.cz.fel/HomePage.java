package cz.cvut.fel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {

    private WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get("https://www.cd.cz/default.htm");
    }

    public void search(String query) {
        driver.findElement(By.id("search-input")).sendKeys(query);
        driver.findElement(By.id("search-button")).click();
    }

    public boolean isHomePageDisplayed() {
        return true;
    }
}
