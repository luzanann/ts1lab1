package cz.cvut.fel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SearchResultsPage {

    private WebDriver driver;

    public SearchResultsPage(WebDriver driver) {
        this.driver = driver;
    }

    public List<WebElement> getResults() {
        return driver.findElements(By.xpath("//div[@id='search-results']//a[@class='result']"));
    }

    public boolean isSearchResultsDisplayed() {
        return true;
    }
}
