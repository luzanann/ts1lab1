package cz.cvut.fel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage {

    private WebDriver driver;

    public CartPage(WebDriver driver) {
        this.driver = driver;
    }

    public int getCartItemCount() {
        String itemCountText = driver.findElement(By.id("cart-item-count")).getText();
        return Integer.parseInt(itemCountText);
    }

    public boolean isCartPageDisplayed() {
        return true;
    }
}
