package cz.cvut.fel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckoutPage {

    private WebDriver driver;

    public CheckoutPage(WebDriver driver) {
        this.driver = driver;
    }

    public void fillShippingAddress(String address, String city, String zipCode) {
        driver.findElement(By.id("shipping-address")).sendKeys(address);
        driver.findElement(By.id("shipping-city")).sendKeys(city);
        driver.findElement(By.id("shipping-zip-code")).sendKeys(zipCode);
    }

    public void placeOrder() {
        driver.findElement(By.id("place-order-button")).click();
    }

    public boolean isCheckoutPageDisplayed() {

        return true;
    }
}
