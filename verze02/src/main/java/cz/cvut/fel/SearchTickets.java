package cz.cvut.fel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchTickets extends Page{
    private WebDriver driver;
    private By inputfrom = By.cssSelector("input.select__input.og-autocomplete.ui-autocomplete-input[name='fasdlk']");
    private By inputto = By.cssSelector("input.select__input.og-autocomplete.ui-autocomplete-input[data-og-type='to']");
    private By button = By.cssSelector("button.btn.btn--filled.btn--green.btn--with-icon.search-btn[data-bind='click: search']");

    public SearchTickets(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public void openSearchTickets() {
        driver.get("https://www.cd.cz/spojeni-a-jizdenka/");
    }
    public void inputfrom(String from) {
        driver.findElement(inputfrom).sendKeys(from);
    }
    public void inputto(String to) {
        driver.findElement(inputto).sendKeys(to);
    }
    public void pressTheButton() {
        driver.findElement(button).click();
    }

}
