package cz.cvut.fel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OrderConfirmationPage {
    private WebElement driver;

    public OrderConfirmationPage(WebDriver driver) {
    }

    public boolean isOrderConfirmationPageDisplayed() {
        WebElement confirmationPage = driver.findElement(By.xpath("//div[@class='order-confirmation-page']"));

        return confirmationPage.isDisplayed();
    }

}
