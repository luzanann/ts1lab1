package cz.cvut.fel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SearchResultsPage {

    private WebDriver driver;
    private By actualfrom = By.cssSelector(".schedule__station > a");
    private By actualto = By.cssSelector("a[data-bind^='attr'][href='https://www.cd.cz/stanice/5453234/'][title='Poděbrady'][aria-label='Poděbrady'][target='_blank']");
    private By button = By.xpath("//*[@id=\"os-simple208306846\"]/div[2]/div/div/div[2]");

    private By buttonToCard = By.xpath("//*[@id=\"main\"]/div[3]/div/div/div[2]/button");
    private boolean areSearchResultsDisplayed;
    private int searchResultsCount;

    public SearchResultsPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean searchTestResult() {
        String expectedFrom = "Praha hl.n.";
        String expectedTo = "Poděbrady";
        String actualFrom = driver.findElement(actualfrom).getText();
        String actualTo = driver.findElement(actualto).getText();
        if (expectedFrom.equals(actualFrom) & expectedTo.equals(actualTo)) {
            return true;
        } else {
            return false;
        }
    }

    public void pressTheButton() {
        driver.findElement(button).click();
    }

    public void pressTheButtonToCard() {
        driver.findElement(buttonToCard).click();
    }

    public int getSearchResultsCount() {
        return searchResultsCount;
    }

    public boolean isSearchResultsDisplayed() {
        return areSearchResultsDisplayed;
    }

}
