package cz.cvut.fel;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class MyTravelPage extends Page {
    private WebDriver driver;

    private By button1 = By.cssSelector(".travbut > button");
    private By button2 = By.cssSelector(".box-grey ma20 > button");
    private By widgetButton = By.cssSelector(".btn-group > button");
    private By widget = By.cssSelector(".tblue portlet-header ui-widget-header ui-corner-all ui-sortable-handle");

    public MyTravelPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public void openMyTravelPage() {
        driver.get("https://www.cd.cz/en/moje-cestovani/");
    }

    public void pressTheButton1() {
        driver.findElement(button1).click();
    }

    public void addWidget() {
        driver.findElement(widgetButton);
    }

    public void pressTheButton2() {

    }

    public boolean resultCheck() {
        if (driver.findElement(widget).isDisplayed()) {
            return true;
        } else {
            return false;
        }
    }
}