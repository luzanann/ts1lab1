package cz.cvut.fel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegistrationPage {

    private WebDriver driver;
    private byte[] passwordErrorText;
    private byte[] emailErrorText;

    public RegistrationPage(WebDriver driver) {
        this.driver = driver;
    }

    public void register(String username, String email, String password) {
        driver.findElement(By.id("username")).sendKeys(username);
        driver.findElement(By.id("email")).sendKeys(email);
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.id("register-button")).click();
    }

    public boolean isRegistrationPageDisplayed() {
        return driver.getCurrentUrl().contains("registration");
    }

    public void clickRegistrationLink() {
        driver.findElement(By.linkText("")).click();
    }

    public boolean isRegistrationSuccessful() {
        return driver.getCurrentUrl().contains("success");
    }

    public void enterFirstName(String firstName) {
        driver.findElement(By.id("firstName")).sendKeys(firstName);
    }

    public void enterLastName(String lastName) {
        driver.findElement(By.id("lastName")).sendKeys(lastName);
    }

    public void enterEmail(String email) {

        driver.findElement(By.id("email")).sendKeys(email);
    }

    public void enterPassword(String password) {
        driver.findElement(By.id("password")).sendKeys(password);
    }

    public void submitRegistrationForm() {
        driver.findElement(By.id("registrationForm")).submit();
    }

    public void submitRegistration() {
        driver.findElement(By.id("registrationButton")).click();
    }

    public String getPasswordErrorText() {
        return driver.findElement(By.id("passwordError")).getText();
    }

    public String getEmailErrorText() {
        return driver.findElement(By.id("emailError")).getText();
    }
}

