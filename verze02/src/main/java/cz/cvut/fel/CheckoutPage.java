package cz.cvut.fel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CheckoutPage {

    private WebDriver driver;
    private boolean isOrderSuccessful;
    private String orderId;

    public CheckoutPage(WebDriver driver) {
        this.driver = driver;
    }

    public void fillShippingAddress(String address, String city, String zipCode) {
        driver.findElement(By.id("shipping-address")).sendKeys(address);
        driver.findElement(By.id("shipping-city")).sendKeys(city);
        driver.findElement(By.id("shipping-zip-code")).sendKeys(zipCode);
    }

    public void placeOrder() {
        driver.findElement(By.id("place-order-button")).click();
    }

    public boolean isCheckoutPageDisplayed() {

        return true;
    }

    public void enterFirstName(String firstName) {
        driver.findElement(By.id("firstNameField")).sendKeys(firstName);
    }

    public String enterLastName(String lastName) {
        driver.findElement(By.id("lastNameField")).sendKeys(lastName);
        return lastName;
    }

    public void enterEmail(String email) {
        driver.findElement(By.id("emailField")).sendKeys(email);
    }

    public void enterPhone(String phone) {
        driver.findElement(By.id("phoneField")).sendKeys(phone);
    }

    public boolean isOrderSuccessful() {
        boolean isOrderSuccessful = false;
        WebElement confirmationMessage = driver.findElement(By.xpath("//div[@class='confirmation-message']"));

        if (confirmationMessage.isDisplayed() && confirmationMessage.getText().equals("Your order has been successfully placed.")) {
            isOrderSuccessful = true;
        }
        String orderStatus = enterLastName(orderId);
        if (orderStatus.equals("Completed")) {
            isOrderSuccessful = true;
        }
        return isOrderSuccessful;
    }


}
