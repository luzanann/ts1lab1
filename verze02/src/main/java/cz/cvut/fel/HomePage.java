package cz.cvut.fel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {

    private WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get("https://www.cd.cz/default.htm");
    }

    public void search(String query) {
        driver.findElement(By.id("search-input")).sendKeys(query);
        driver.findElement(By.id("search-button")).click();
    }

    public boolean isHomePageDisplayed() {
        return true;
    }

    public void acceptCookies() {
    }

    public void enterFromStation(String from) {
    }

    public void enterToStation(String to) {
    }

    public void submitSearch() {
    }

    public ProductPage searchProduct(String productName) {
        driver.findElement(By.id("searchInput")).sendKeys(productName);
        driver.findElement(By.id("searchButton")).click();
        return new ProductPage(driver);
    }

    public RegistrationPage clickRegistrationLink() {
        driver.findElement(By.linkText("Registration")).click();
        return new RegistrationPage(driver);
    }

    public LoginPage clickLoginLink() {
        driver.findElement(By.linkText("Login")).click();
        return new LoginPage(driver);
    }
}
