package cz.cvut.fel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Page {
    protected WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public void acceptCookies() {
        try {
            WebElement acceptButton = driver.findElement(By.cssSelector("button.btn.btnallow#consentBtnall"));
            acceptButton.click();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

