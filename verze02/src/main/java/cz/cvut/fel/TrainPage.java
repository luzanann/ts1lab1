package cz.cvut.fel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TrainPage extends Page{
    private WebDriver driver;

    private By train = By.cssSelector("input#trainName.dropdown-toggle.wide.og-autocomplete.ui-autocomplete-input[name='trainName']");
    private By button = By.cssSelector("button#cmdSearch");
    private By result = By.cssSelector(".train-name");
//    private By widget = ;

    public TrainPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public void openMyTrainPage() {
        driver.get("https://www.cd.cz/vlak/");
    }

    public void selectTrain(String Train) {
        driver.findElement(train).sendKeys(Train);
    }

    public void pressTheButton() {
        driver.findElement(button).click();
    }

    public boolean resultCheking() {
        String expectedText = "R 957 Hradečan\n" +
                "Praha hl.n. - Hradec Králové hl.n.";
        String actualText = driver.findElement(result).getText();
        System.out.println(actualText);
        if (expectedText.equals(actualText)) {
            return true;
        } else {
            return false;
        }
    }
}