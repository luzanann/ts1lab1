package cz.cvut.fel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends Page{

    private WebDriver driver;
    private By email = By.cssSelector("input.cd-input-def#username");
    private By pass = By.cssSelector("input.cd-input-def#password");
    private By button = By.cssSelector("button.cd-btn-green.floatright");

    public LoginPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }
    public void openLogInPage() {
        driver.get("https://www.cd.cz/profil-uzivatele/auth/login");
    }
    public void inputEmail(String username) {
        driver.findElement(email).sendKeys(username);
    }
    public void inputPassword(String password) {
        driver.findElement(pass).sendKeys(password);
    }
    public void pressTheButton() {
        driver.findElement(button).click();
    }

    public boolean logInTestResult() {
        String expectedTitle = "Profil uživatele | České dráhy";
        String actualTitle = driver.getTitle();
        if (expectedTitle.equals(actualTitle)) {
            return true;
        } else {
            return false;
        }
    }

    public void clickRegistrationLink() {
        WebElement registrationLink = driver.findElement(By.linkText("Регистрация"));
        registrationLink.click();
    }

    public void enterUsername(String username) {
        WebElement usernameField = driver.findElement(By.id("username"));
        usernameField.sendKeys(username);
    }

    public void enterPassword(String password) {

        WebElement passwordField = driver.findElement(By.id("password"));
        passwordField.sendKeys(password);
    }

    public void submitLogin() {
        WebElement loginButton = driver.findElement(By.cssSelector("button[type='submit']"));
        loginButton.click();
    }


    public boolean isLoginPageDisplayed() {
        boolean isLoginPageDisplayed = false;
        if (driver.findElement(By.id("loginForm")).isDisplayed()) {
            isLoginPageDisplayed = true;
        }
        return isLoginPageDisplayed;
    }

}
