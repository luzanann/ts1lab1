package cz.cvut.fel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class UserAccountPage {

    private WebDriver driver;
    private boolean isLoggedIn;

    public UserAccountPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getUsername() {
        return driver.findElement(By.id("username")).getText();
    }

    public boolean isUserAccountPageDisplayed() {
        return true;
    }

    public boolean isLoggedIn() {
        boolean isLoggedIn = false;
        if (driver.findElement(By.id("logoutButton")).isDisplayed()) {
            isLoggedIn = true;
        }
        return isLoggedIn;
    }
}
