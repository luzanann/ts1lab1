package cz.cvut.fel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CartPage extends Page {

    private WebDriver driver;
    private By result = By.cssSelector("button.btn.btn--filled.btn--green[data-bind*='enable'][data-bind*='click']");
    ;

    public CartPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public void openCartPage() {
        driver.get("https://www.cd.cz/eshop/kosik/");
    }

    public boolean resultCheck() {
        String expectedText = "Jízdenka";
        String actualText = driver.findElement(result).getText();
        if (expectedText.equals(actualText)) {
            return true;
        } else {
            return false;
        }
    }

    public void proceedToCheckout() {
        driver.findElement(By.id("proceedToCheckoutButton")).click();
    }

    public boolean isCartPageDisplayed() {
        return driver.findElement(By.id("cartPage")).isDisplayed();
    }

}






