
import cz.cvut.fel.*;
import cz.cvut.fel.SearchResultsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.Duration;

public class CSVtest {
    private static WebDriver driver;
    Duration duration = Duration.ofSeconds(10);
    @DataProvider(name = "trains")
    public Object[][] getTrainData() {
        return new Object[][]{
                {"Prague", "Berlin"},
                {"Brno", "Vienna"},
                {"London", "Paris"}
        };
    }

    @Test(dataProvider = "trains")
    public void testTrainSearch(String from, String to) {
        HomePage homePage = new HomePage(driver);
        homePage.open();
        homePage.acceptCookies();
        homePage.enterFromStation(from);
        homePage.enterToStation(to);
        homePage.submitSearch();
        WebDriverWait wait = new WebDriverWait(driver, duration);
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("results")));
        Assert.assertTrue(searchResultsPage.isSearchResultsDisplayed(), "Search results are not displayed correctly.");
    }

    @DataProvider(name = "emails")
    public Object[][] getEmailFieldData() {
        return new Object[][]{
                {"texttext", "Zadaná e-mailová adresa není ve správném formátu."},
                {";%421-=@gmail.com", "Zadaná e-mailová adresa není ve správném formátu."},
                {"", "Vyplňte prosím e-mail."}
        };
    }

    @Test(dataProvider = "emails")
    public void testRegistrationEmailFormValidation(String email, String expectedErrorText) {
        HomePage homePage = new HomePage(driver);
        homePage.open();
        homePage.acceptCookies();

        LoginPage loginPage = homePage.clickLoginLink();
        loginPage.clickRegistrationLink();

        RegistrationPage registrationPage = new RegistrationPage(driver);
        registrationPage.enterEmail(email);
        registrationPage.submitRegistrationForm();

        Assert.assertEquals(registrationPage.getEmailErrorText(), expectedErrorText, "Email error message is not displayed correctly.");
    }

    @DataProvider(name = "passwords")
    public Object[][] getPasswordFieldData() {
        return new Object[][]{
                {"", "empty", "Vyplňte prosím heslo."},
                {"1", "kratke", "Heslo musí být minimálně 6 znaků dlouhé."}
        };
    }

    @Test(dataProvider = "passwords")
    public void testRegistrationPasswordFormValidation(String password, String name, String expectedErrorText) {
        HomePage homePage = new HomePage(driver);
        homePage.open();
        homePage.acceptCookies();

        LoginPage loginPage = homePage.clickLoginLink();
        loginPage.clickRegistrationLink();

        RegistrationPage registrationPage = new RegistrationPage(driver);
        registrationPage.enterPassword(password);
        registrationPage.submitRegistrationForm();

        Assert.assertEquals(registrationPage.getPasswordErrorText(), expectedErrorText, "Password error message is not displayed correctly.");
    }
    @DataProvider(name = "loginData")
    public Object[][] getLoginData() {
        return new Object[][]{
                {"user1", "password1"},
                {"user2", "password2"},
                {"user3", "password3"},
                {"user4", "password4"},
                {"user5", "password5"}
        };
    }

    @Test(dataProvider = "loginData")
    public void testLogin(String username, String password) {
        HomePage homePage = new HomePage(driver);
        homePage.open();
        homePage.acceptCookies();

        LoginPage loginPage = homePage.clickLoginLink();
        loginPage.enterUsername(username);
        loginPage.enterPassword(password);
        loginPage.submitLogin();

        UserAccountPage userAccountPage = new UserAccountPage(driver);
        Assert.assertTrue(userAccountPage.isLoggedIn(), "Login failed for user: " + username);
    }

    @DataProvider(name = "searchData")
    public Object[][] getSearchData() {
        return new Object[][]{
                {"Prague", "Berlin", 2},
                {"Brno", "Vienna", 3},
                {"London", "Paris", 4},
                {"Rome", "Florence", 1},
                {"Barcelona", "Madrid", 5}
        };
    }

    @Test(dataProvider = "searchData")
    public void testTrainSearch(String from, String to, int expectedResults) {
        HomePage homePage = new HomePage(driver);
        homePage.open();
        homePage.acceptCookies();
        homePage.enterFromStation(from);
        homePage.enterToStation(to);
        homePage.submitSearch();

        WebDriverWait wait = new WebDriverWait(driver, duration);
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("results")));
        Assert.assertEquals(searchResultsPage.getSearchResultsCount(), expectedResults, "Search results count is incorrect.");
    }

    @DataProvider(name = "registrationData")
    public Object[][] getRegistrationData() {
        return new Object[][]{
                {"John", "Doe", "john.doe@example.com", "P@ssw0rd"},
                {"Jane", "Smith", "jane.smith@example.com", "P@ssw0rd"},
                {"Mike", "Johnson", "mike.johnson@example.com", "P@ssw0rd"},
                {"Emily", "Williams", "emily.williams@example.com", "P@ssw0rd"},
                {"David", "Brown", "david.brown@example.com", "P@ssw0rd"}
        };
    }

    @Test(dataProvider = "registrationData")
    public void testRegistration(String firstName, String lastName, String email, String password) {
        HomePage homePage = new HomePage(driver);
        homePage.open();
        homePage.acceptCookies();

        RegistrationPage registrationPage = homePage.clickRegistrationLink();
        registrationPage.enterFirstName(firstName);
        registrationPage.enterLastName(lastName);
        registrationPage.enterEmail(email);
        registrationPage.enterPassword(password);
        registrationPage.submitRegistration();

        UserAccountPage userAccountPage = new UserAccountPage(driver);
        Assert.assertTrue(userAccountPage.isLoggedIn(), "Registration failed for user: " + firstName + " " + lastName);
    }

    @DataProvider(name = "purchaseData")
    public Object[][] getPurchaseData() {
        return new Object[][]{
                {"Product1", 1, "John", "Doe", "john.doe@example.com", "1234567890"},
                {"Product2", 2, "Jane", "Smith", "jane.smith@example.com", "9876543210"},
                {"Product3", 3, "Mike", "Johnson", "mike.johnson@example.com", "5678901234"},
                {"Product4", 4, "Emily", "Williams", "emily.williams@example.com", "4321098765"},
                {"Product5", 5, "David", "Brown", "david.brown@example.com", "6789054321"}
        };
    }

    @Test(dataProvider = "purchaseData")
    public void testPurchase(String productName, int quantity, String firstName, String lastName, String email, String phone) {
        HomePage homePage = new HomePage(driver);
        homePage.open();
        homePage.acceptCookies();

        ProductPage productPage = homePage.searchProduct(productName);
        productPage.selectQuantity(quantity);
        productPage.addToCart();

        CartPage cartPage = new CartPage(driver);
        cartPage.proceedToCheckout();

        CheckoutPage checkoutPage = new CheckoutPage(driver);
        checkoutPage.enterFirstName(firstName);
        checkoutPage.enterLastName(lastName);
        checkoutPage.enterEmail(email);
        checkoutPage.enterPhone(phone);
        checkoutPage.placeOrder();

        Assert.assertTrue(checkoutPage.isOrderSuccessful(), "Order placement failed for product: " + productName);
    }

}
