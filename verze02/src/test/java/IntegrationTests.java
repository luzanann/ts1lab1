import cz.cvut.fel.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;

import java.time.Duration;

class IntegrationTests {
    private WebDriver driver;
    @BeforeAll
    static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupTest() {
        driver = new ChromeDriver();
    }

    @AfterEach
    void teardown() {
        driver.quit();
    }
    @Test
    public void testHomePage() {
        // Open the website
        driver.get("https://www.cd.cz/default.htm");

        // Validate the home page
        HomePage homePage = new HomePage(driver);
        Assertions.assertTrue(homePage.isHomePageDisplayed());
    }

    @Test
    public void testSearchResultsPage() {
        driver.get("https://www.cd.cz/default.htm");

        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
        Assertions.assertTrue(searchResultsPage.isSearchResultsDisplayed());
    }


    @Test
    public void testRegistrationPage() {
        driver.get("https://www.cd.cz/default.htm");
        RegistrationPage registrationPage = new RegistrationPage(driver);
        Assertions.assertTrue(registrationPage.isRegistrationPageDisplayed());
    }

    @Test
    public void testProductPage() {
        driver.get("https://www.cd.cz/default.htm");

        ProductPage productPage = new ProductPage(driver);
        Assertions.assertTrue(productPage.isProductPageDisplayed());
    }

    @Test
    public void testCartPage() {
        driver.get("https://www.cd.cz/default.htm");
        CartPage cartPage = new CartPage(driver);
        Assertions.assertTrue(cartPage.isCartPageDisplayed());
    }

    @Test
    public void testCheckoutPage() {
        driver.get("https://www.cd.cz/default.htm");
        CheckoutPage checkoutPage = new CheckoutPage(driver);
        Assertions.assertTrue(checkoutPage.isCheckoutPageDisplayed());
    }

    @Test
    public void testUserAccountPage() {
        driver.get("https://www.cd.cz/default.htm");
        UserAccountPage userAccountPage = new UserAccountPage(driver);
        Assertions.assertTrue(userAccountPage.isUserAccountPageDisplayed());
    }
}
