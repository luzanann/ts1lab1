import cz.cvut.fel.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.io.File;
import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static org.testng.AssertJUnit.assertEquals;

public class cdCzTest {

    private WebDriver driver;
    int duration20 = 30;
    int duration5 = 5;
    Duration duration = Duration.ofSeconds((10));
    @BeforeAll
    static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupTest() {
        driver = new ChromeDriver();
    }

    @AfterEach
    void teardown() {
        driver.quit();
    }

    @Test
    public void logInTest() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openLogInPage();
        loginPage.acceptCookies();
        loginPage.inputEmail("anna2004luzan@gmail.com");
        loginPage.inputPassword("anna20042310");
        loginPage.pressTheButton();
        Assert.assertTrue(loginPage.logInTestResult(), "Error");
    }

    @Test
    public void SearchTickets() {
        SearchTickets searchTickets = new SearchTickets(driver);
        WebDriverWait wait = new WebDriverWait(driver, duration);
        searchTickets.openSearchTickets();
        searchTickets.acceptCookies();
        searchTickets.inputfrom("Praha hl.n.");
        searchTickets.inputto("Poděbrady");
        searchTickets.pressTheButton();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".schedule__station > a")));
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
        Assert.assertTrue(searchResultsPage.searchTestResult(),"Error");
    }

    @Test
    public void TrainSelectTest() {
        TrainPage trainPage = new TrainPage(driver);
        trainPage.openMyTrainPage();
        trainPage.acceptCookies();
        trainPage.selectTrain("R 957 Hradečan");
        trainPage.pressTheButton();
        Assert.assertTrue(trainPage.resultCheking(), "Error");
    }


    @Test
    public void addToShipCard() throws InterruptedException {
        SearchTickets searchTickets = new SearchTickets(driver);
        WebDriverWait wait = new WebDriverWait(driver, duration);
        searchTickets.openSearchTickets();
        searchTickets.acceptCookies();
        searchTickets.inputfrom("Praha hl.n.");
        searchTickets.inputto("Poděbrady");
        searchTickets.pressTheButton();
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
//        wait.until(ExpectedConditions.visibilityOfElementLocated());
        TimeUnit.SECONDS.sleep(duration20);
        searchResultsPage.pressTheButton();
        searchResultsPage.pressTheButtonToCard();
        CartPage cartPage = new CartPage(driver);
        Assert.assertTrue(cartPage.resultCheck(), "Error");
    }

    @Test
    public void widgetTest() throws InterruptedException {
        MyTravelPage myTravelPage = new MyTravelPage(driver);
        myTravelPage.openMyTravelPage();
        myTravelPage.acceptCookies();
        TimeUnit.SECONDS.sleep(duration5);
        myTravelPage.pressTheButton1();
        TimeUnit.SECONDS.sleep(duration5);
        myTravelPage.addWidget();
        TimeUnit.SECONDS.sleep(duration20);
        myTravelPage.pressTheButton2();
        Assert.assertTrue(myTravelPage.resultCheck(), "Error");
    }

    @Test
    public void testAddTripToFavorites() {
        driver.get("https://www.cd.cz/default.htm");
        WebElement fromStation = driver.findElement(By.id("fromStation"));
        fromStation.sendKeys("Praha");

        WebElement toStation = driver.findElement(By.id("toStation"));
        toStation.sendKeys("Brno");

        WebElement dateInput = driver.findElement(By.id("datepicker"));
        dateInput.sendKeys("2023-07-01");

        WebElement searchButton = driver.findElement(By.xpath("//input[@value='Vyhledat']"));
        searchButton.click();


        WebElement firstTrainLink = driver.findElement(By.xpath("//a[contains(@href,'SelectTrainsForm-submit')]"));
        firstTrainLink.click();

        WebElement addToFavoritesButton = driver.findElement(By.xpath("//a[contains(@class,'addToFavourite')]"));
        addToFavoritesButton.click();

        WebElement favoritesLink = driver.findElement(By.xpath("//a[contains(@href,'MyFavorites')][@title='Moje oblíbené']"));
        favoritesLink.click();

        WebElement tripTitle = driver.findElement(By.xpath("//h3[@class='trip-name']"));
        assertEquals("Praha do Brna", tripTitle.getText());
    }

    @Test
    public void verifyLanguageSelection() {
        driver.get("https://www.cd.cz/default.htm");
        WebElement languageDropdown = driver.findElement(By.id("dropdownToggleButton"));
        languageDropdown.click();
        WebElement languageOption = driver.findElement(By.xpath("//a[@class='dropdown-menu__link' and contains(@href, 'setlang?lang=en') and contains(@title, 'Switch to english')]"));
        WebElement pageTitle = driver.findElement(By.xpath("//h1[contains(text(),'Welcome')]"));
        Assert.assertEquals(pageTitle.getText(), "Welcome", "Language selection is not working correctly.");
    }
    @Test
    public void testHomePage() {
        // Open the website
        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");

        // Validate the home page
        HomePage homePage = new HomePage(driver);
        Assertions.assertTrue(homePage.isHomePageDisplayed());

        // Close the browser
        driver.quit();
    }

    @Test
    public void testSearchResultsPage() {

        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");
        // Validate the search results page
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
        Assertions.assertTrue(searchResultsPage.isSearchResultsDisplayed());


        driver.quit();
    }

    @Test
    public void testLoginPage() {

        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");


        LoginPage loginPage = new LoginPage(driver);
        Assertions.assertTrue(loginPage.isLoginPageDisplayed());


        driver.quit();
    }


    @Test
    public void testProductPage() {

        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");



        ProductPage productPage = new ProductPage(driver);
        Assertions.assertTrue(productPage.isProductPageDisplayed());


        driver.quit();
    }

    @Test
    public void testCartPage() {

        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");

        CartPage cartPage = new CartPage(driver);
        Assertions.assertTrue(cartPage.isCartPageDisplayed());


        driver.quit();
    }

    @Test
    public void testCheckoutPage() {

        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");


        CheckoutPage checkoutPage = new CheckoutPage(driver);
        Assertions.assertTrue(checkoutPage.isCheckoutPageDisplayed());

        driver.quit();
    }

    @Test
    public void testOrderConfirmationPage() {

        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");

        OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(driver);
        Assertions.assertTrue(orderConfirmationPage.isOrderConfirmationPageDisplayed());


        driver.quit();
    }

    @Test
    public void testUserAccountPage() {

        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");

        UserAccountPage userAccountPage = new UserAccountPage(driver);
        Assertions.assertTrue(userAccountPage.isUserAccountPageDisplayed());


        driver.quit();
    }


    @Test
    public void testMethod1() {
        int result = calculateSum(2, 3);
        Assert.assertEquals(result, 5);
    }

    @Test
    public void testMethod2() {
        String fullName = createFullName("John", "Doe");
        Assert.assertEquals(fullName, "John Doe");
    }

    @Test
    public void testMethod3() {
        boolean isPositive = checkPositiveNumber(10);
        Assert.assertTrue(isPositive);
    }

    @Test
    public void testMethod4() {
        boolean isEven = checkEvenNumber(4);
        Assert.assertTrue(isEven);
    }

    @Test
    public void testMethod5() {
        int[] numbers = {1, 2, 3, 4, 5};
        int sum = calculateArraySum(numbers);
        Assert.assertEquals(sum, 15);
    }

    @Test
    public void testMethod6() {
        String reversed = reverseString("Hello");
        Assert.assertEquals(reversed, "olleH");
    }

    @Test
    public void testMethod7() {
        int factorial = calculateFactorial(5);
        Assert.assertEquals(factorial, 120);
    }

    @Test
    public void testMethod8() {
        String[] names = {"Alice", "Bob", "Charlie"};
        boolean containsName = checkNameExists(names, "Bob");
        Assert.assertTrue(containsName);
    }

    @Test
    public void testMethod9() {
        int[] sortedArray = sortArray(new int[]{5, 3, 1, 4, 2});
        int[] expectedArray = {1, 2, 3, 4, 5};
        Assert.assertEquals(sortedArray, expectedArray);
    }

    @Test
    public void testMethod10() {
        String greeting = getGreeting("en");
        Assert.assertEquals(greeting, "Hello");
    }

    private int calculateSum(int a, int b) {
        return a + b;
    }

    private String createFullName(String firstName, String lastName) {
        return firstName + " " + lastName;
    }

    private boolean checkPositiveNumber(int number) {
        return number > 0;
    }

    private boolean checkEvenNumber(int number) {
        return number % 2 == 0;
    }

    private int calculateArraySum(int[] numbers) {
        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }
        return sum;
    }

    private String reverseString(String str) {
        StringBuilder reversed = new StringBuilder(str);
        return reversed.reverse().toString();
    }

    private int calculateFactorial(int number) {
        int factorial = 1;
        for (int i = 1; i <= number; i++) {
            factorial *= i;
        }
        return factorial;
    }

    private boolean checkNameExists(String[] names, String targetName) {
        for (String name : names) {
            if (name.equals(targetName)) {
                return true;
            }
        }
        return false;
    }

    private int[] sortArray(int[] array) {
        int[] sortedArray = array.clone();
        Arrays.sort(sortedArray);
        return sortedArray;
    }

    private String getGreeting(String language) {
        if (language.equals("en")) {
            return "Hello";
        } else if (language.equals("fr")) {
            return "Bonjour";
        } else if (language.equals("es")) {
            return "Hola";
        } else {
            return "Unknown";
        }
    }

}


