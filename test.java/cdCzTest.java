import cz.cvut.fel.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterTest;

import java.time.Duration;
import java.util.Arrays;

public class cdCzTest {

    private WebDriver driver;

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "/Users/aleksandr/Documents/chromedriver");
        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");

        // Wait for page to load
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("body")));
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testHomePage() {
        // Open the website
        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");

        // Validate the home page
        HomePage homePage = new HomePage(driver);
        Assertions.assertTrue(homePage.isHomePageDisplayed());

        // Close the browser
        driver.quit();
    }

    @Test
    public void testSearchResultsPage() {
        // Open the website
        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");
        // Validate the search results page
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
        Assertions.assertTrue(searchResultsPage.isSearchResultsDisplayed());

        // Close the browser
        driver.quit();
    }

    @Test
    public void testLoginPage() {
        // Open the website
        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");

        // Validate the login page
        LoginPage loginPage = new LoginPage(driver);
        Assertions.assertTrue(loginPage.isLoginPageDisplayed());

        // Close the browser
        driver.quit();
    }
    

    @Test
    public void testProductPage() {
        // Open the website
        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");


        // Validate the product page
        ProductPage productPage = new ProductPage(driver);
        Assertions.assertTrue(productPage.isProductPageDisplayed());

        // Close the browser
        driver.quit();
    }

    @Test
    public void testCartPage() {
        // Open the website
        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");


        // Validate the cart page
        CartPage cartPage = new CartPage(driver);
        Assertions.assertTrue(cartPage.isCartPageDisplayed());

        // Close the browser
        driver.quit();
    }

    @Test
    public void testCheckoutPage() {
        // Open the website
        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");


        CheckoutPage checkoutPage = new CheckoutPage(driver);
        Assertions.assertTrue(checkoutPage.isCheckoutPageDisplayed());

        driver.quit();
    }

    @Test
    public void testOrderConfirmationPage() {
        // Open the website
        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");
        // Validate the order confirmation page
        OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(driver);
        Assertions.assertTrue(orderConfirmationPage.isOrderConfirmationPageDisplayed());

        // Close the browser
        driver.quit();
    }

    @Test
    public void testUserAccountPage() {
        // Open the website
        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");

        // Validate the user account page
        UserAccountPage userAccountPage = new UserAccountPage(driver);
        Assertions.assertTrue(userAccountPage.isUserAccountPageDisplayed());

        // Close the browser
        driver.quit();
    }


    @Test
    public void testMethod1() {
        int result = calculateSum(2, 3);
        Assert.assertEquals(result, 5);
    }

    @Test
    public void testMethod2() {
        String fullName = createFullName("John", "Doe");
        Assert.assertEquals(fullName, "John Doe");
    }

    @Test
    public void testMethod3() {
        boolean isPositive = checkPositiveNumber(10);
        Assert.assertTrue(isPositive);
    }

    @Test
    public void testMethod4() {
        boolean isEven = checkEvenNumber(4);
        Assert.assertTrue(isEven);
    }

    @Test
    public void testMethod5() {
        int[] numbers = {1, 2, 3, 4, 5};
        int sum = calculateArraySum(numbers);
        Assert.assertEquals(sum, 15);
    }

    @Test
    public void testMethod6() {
        String reversed = reverseString("Hello");
        Assert.assertEquals(reversed, "olleH");
    }

    @Test
    public void testMethod7() {
        int factorial = calculateFactorial(5);
        Assert.assertEquals(factorial, 120);
    }

    @Test
    public void testMethod8() {
        String[] names = {"Alice", "Bob", "Charlie"};
        boolean containsName = checkNameExists(names, "Bob");
        Assert.assertTrue(containsName);
    }

    @Test
    public void testMethod9() {
        int[] sortedArray = sortArray(new int[]{5, 3, 1, 4, 2});
        int[] expectedArray = {1, 2, 3, 4, 5};
        Assert.assertEquals(sortedArray, expectedArray);
    }

    @Test
    public void testMethod10() {
        String greeting = getGreeting("en");
        Assert.assertEquals(greeting, "Hello");
    }

    private int calculateSum(int a, int b) {
        return a + b;
    }

    private String createFullName(String firstName, String lastName) {
        return firstName + " " + lastName;
    }

    private boolean checkPositiveNumber(int number) {
        return number > 0;
    }

    private boolean checkEvenNumber(int number) {
        return number % 2 == 0;
    }

    private int calculateArraySum(int[] numbers) {
        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }
        return sum;
    }

    private String reverseString(String str) {
        StringBuilder reversed = new StringBuilder(str);
        return reversed.reverse().toString();
    }

    private int calculateFactorial(int number) {
        int factorial = 1;
        for (int i = 1; i <= number; i++) {
            factorial *= i;
        }
        return factorial;
    }

    private boolean checkNameExists(String[] names, String targetName) {
        for (String name : names) {
            if (name.equals(targetName)) {
                return true;
            }
        }
        return false;
    }

    private int[] sortArray(int[] array) {
        int[] sortedArray = array.clone();
        Arrays.sort(sortedArray);
        return sortedArray;
    }

    private String getGreeting(String language) {
        if (language.equals("en")) {
            return "Hello";
        } else if (language.equals("fr")) {
            return "Bonjour";
        } else if (language.equals("es")) {
            return "Hola";
        } else {
            return "Unknown";
        }
    }

}

