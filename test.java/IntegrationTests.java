import cz.cvut.fel.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;

import java.time.Duration;

class IntegrTest {
    private WebDriver driver;
    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "/Users/aleksandr/Documents/chromedriver");
        driver = new ChromeDriver();
        driver.get("https://www.cd.cz/default.htm");

        // Wait for page to load
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("body")));
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }
    @Test
    public void testHomePage() {
        // Open the website
        driver.get("https://www.cd.cz/default.htm");

        // Validate the home page
        HomePage homePage = new HomePage(driver);
        Assertions.assertTrue(homePage.isHomePageDisplayed());
    }

    @Test
    public void testSearchResultsPage() {
        // Open the website
        driver.get("https://www.cd.cz/default.htm");

        // Validate the search results page
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
        Assertions.assertTrue(searchResultsPage.isSearchResultsDisplayed());
    }

    @Test
    public void testLoginPage() {
        // Open the website
        driver.get("https://www.cd.cz/default.htm");

        // Validate the login page
        LoginPage loginPage = new LoginPage(driver);
        Assertions.assertTrue(loginPage.isLoginPageDisplayed());
    }

    @Test
    public void testRegistrationPage() {
        // Open the website
        driver.get("https://www.cd.cz/default.htm");
        // Validate the registration page
        RegistrationPage registrationPage = new RegistrationPage(driver);
        Assertions.assertTrue(registrationPage.isRegistrationPageDisplayed());
    }

    @Test
    public void testProductPage() {
        // Open the website
        driver.get("https://www.cd.cz/default.htm");

        // Validate the product page
        ProductPage productPage = new ProductPage(driver);
        Assertions.assertTrue(productPage.isProductPageDisplayed());
    }

    @Test
    public void testCartPage() {
        // Open the website
        driver.get("https://www.cd.cz/default.htm");

        // Validate the cart page
        CartPage cartPage = new CartPage(driver);
        Assertions.assertTrue(cartPage.isCartPageDisplayed());
    }

    @Test
    public void testCheckoutPage() {
        // Open the website
        driver.get("https://www.cd.cz/default.htm");


        // Validate the checkout page
        CheckoutPage checkoutPage = new CheckoutPage(driver);
        Assertions.assertTrue(checkoutPage.isCheckoutPageDisplayed());
    }

    @Test
    public void testUserAccountPage() {
        // Open the website
        driver.get("https://www.cd.cz/default.htm");


        // Validate the user account page
        UserAccountPage userAccountPage = new UserAccountPage(driver);
        Assertions.assertTrue(userAccountPage.isUserAccountPageDisplayed());
    }
}
